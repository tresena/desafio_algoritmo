FROM scratch

WORKDIR /structure

ADD layer.tar.xz /.

CMD [ "/bin/bash" ]

USER Rocky